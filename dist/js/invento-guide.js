(function(d, a) {
    'use strict'

    var inventoGuide = angular.module('invento-guide', []);

    inventoGuide.controller('InventoGuideLogout', function($scope, $location) {
        $scope.logout = function() {
            $('.invento-guide-wrapper').remove();
            $location.path('/logout');
        };
    });

    inventoGuide.provider('InventoGuideBuilder', function() {
        var GuideBuilder = {};

        this.HEADER_TITLE     = 'Welcome to LettingHQ';
        this.MAX_PAGES        = 1;
        this.PRELOAD_REQUEST  = null;
        this.PRODUCT_NAME     = 'LettingHQ';
        this.CONTACT_EMAIL    = 'hello@lettinglead.com';

        this.setHeader = function(title) {
            this.HEADER_TITLE = title;

            return this;
        }

        this.setPages = function(count) {
            this.MAX_PAGES = parseInt(count);

            return this;
        };

        this.setPreloadRequest = function(resource) {
            this.PRELOAD_REQUEST = resource;

            return this;
        }

        this.$get = ['$rootScope', '$templateRequest', '$compile', function($rootScope, $templateRequest, $compile, $location) {
            var pub = {
                build : function() {
                    $('body').addClass('no-scroll');
                    var base = $('<div></div>').addClass('invento-guide-wrapper').appendTo('body');

                    $templateRequest('bower_components/invento-onboarding/dist/html/template.html').then(function(template) {
                        $compile(base.html(template).contents())($rootScope);
                        base.find('.header h2').text(pub.headerTitle);
                        base.find('.support-email').attr('href', ('mailto:' + pub.contactEmail));

                        $templateRequest('views/onboarding/content.html', true).then(function(template) {
                            var content = base.find('.content');

                            $compile(content.html(template).contents())($rootScope);

                            base.show('fast');

                            pub.preloadRequest.get(function(success) {
                                $rootScope.$broadcast('OnboardingLoadedPreRequisites', success.response.data);
                            }, function(error) {
                                $rootScope.$broadcast('OnboardingLoadedPreRequisites', {});
                            });
                        });
                    });
                },

                currentPage     : 1,
                maxPage         : this.MAX_PAGES,
                headerTitle     : this.HEADER_TITLE,
                preloadRequest  : this.PRELOAD_REQUEST,
                productName     : this.PRODUCT_NAME,
                contactEmail    : this.CONTACT_EMAIL,
                getProductName  : function() {
                    return pub.productName;
                },
                setProductName  : function(name) {
                    pub.productName = name;
                },
                setHeader       : function(title) {
                    var base = $('.invento-guide-wrapper');

                    pub.headerTitle = title;
                    base.find('.header h2').text(title);
                },
                getContactEmail : function() {
                    return pub.contactEmail;
                },
                setContactEmail : function(email) {
                    pub.contactEmail = email;
                    $('.support-email').attr('href', 'mailto:' + email);
                }
            };

            return pub;
        }];
    });
})(document, angular);